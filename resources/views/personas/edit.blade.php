<h2>Editar Persona</h2>

<form action="{{ route('personas.update', $persona->id) }}" method="POST">
    @csrf
    @method('PUT')
    <label for="nombre">Nombre:</label>
    <input type="text" id="nombre" name="nombre" value="{{ $persona->nombre }}" required>
    
    <label for="apellido">Apellido:</label>
    <input type="text" id="apellido" name="apellido" value="{{ $persona->apellido }}" required>
    
    <label for="edad">Edad:</label>
    <input type="number" id="edad" name="edad" value="{{ $persona->edad }}" required>
    
    <label for="email">Email:</label>
    <input type="email" id="email" name="email" value="{{ $persona->email }}" required>
         
    <button type="submit">Actualizar</button>
</form>
