<!DOCTYPE html>
<html>
<head>
    <title>Formulario y tabla</title>
    <style>
        table {
            border-collapse: collapse;
            width: 100%;
        }
        
        th, td {
            padding: 8px;
            text-align: left;
            border-bottom: 1px solid #ddd;
        }
        
        form {
            margin-bottom: 20px;
        }
        
        button {
            padding: 8px 16px;
            background-color: #4CAF50;
            color: white;
            border: none;
            cursor: pointer;
        }
        
        button:hover {
            background-color: #45a049;
        }
    </style>
</head>
<body>
    <h2>Formulario</h2>
    <form>
        <label for="nombre">Nombre:</label>
        <input type="text" id="nombre" name="nombre" required>
        
        <label for="apellido">Apellido:</label>
        <input type="text" id="apellido" name="apellido" required>

        <label for="edad">Edad:</label>
        <input type="number" id="edad" name="edad" required>

        <label for="email">Email:</label>
        <input type="email" id="email" name="email" required>
        
        <button type="submit">Agregar</button>
    </form>
    
    <h2>Tabla</h2>
    <table>
        <thead>
            <tr>
                <th>Nombre</th>
                <th>Email</th>
                <th>Acciones</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>John Doe</td>
                <td>johndoe@example.com</td>
                <td>
                    <button>Modificar</button>
                    <button>Eliminar</button>
                </td>
            </tr>
            <!-- Puedes agregar más filas según tus necesidades -->
        </tbody>
    </table>
</body>
</html>
