<h2>Agregar Persona</h2>

<form action="{{ route('personas.store') }}" method="POST">
    @csrf
    <label for="nombre">Nombre:</label>
    <input type="text" id="nombre" name="nombre" required>
    
    <label for="apellido">Apellido:</label>
    <input type="text" id="apellido" name="apellido" required>
    
    <label for="edad">Edad:</label>
    <input type="number" id="edad" name="edad" required>
    
    <label for="email">Email:</label>
    <input type="email" id="email" name="email" required>
    
    <button type="submit">Agregar</button>
</form>
