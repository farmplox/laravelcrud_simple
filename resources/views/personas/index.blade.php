<h2>Listado de Personas</h2>
<a href="{{ route('personas.create') }}">Agregar Persona</a>

<table>
    <thead>
        <tr>
            <th>ID</th>
            <th>Nombre</th>
            <th>Apellido</th>
            <th>Edad</th>
            <th>Email</th>
            <th>Acciones</th>
        </tr>
    </thead>
    <tbody>
        @foreach($personas as $persona)
        <tr>
            <td>{{ $persona->id }}</td>
            <td>{{ $persona->nombre }}</td>
            <td>{{ $persona->apellido }}</td>
            <td>{{ $persona->edad }}</td>
            <td>{{ $persona->email }}</td>
            <td>
                <a href="{{ route('personas.edit', $persona->id) }}">Editar</a>
                <form action="{{ route('personas.destroy', $persona->id) }}" method="POST">
                    @csrf
                    @method('DELETE')
                    <button type="submit">Eliminar</button>
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
